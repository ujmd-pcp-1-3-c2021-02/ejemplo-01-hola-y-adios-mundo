using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ejemplo_HolaMundo_1_3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label1.Text = "Hola Mundo de PCP0 1-3";
            label1.BackColor = Color.DodgerBlue;
            label1.ForeColor = Color.White;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            label1.Text = "Adiós Mundo de PCP0 1-3";
            label1.BackColor = Color.Firebrick;
            label1.ForeColor = Color.White;
        }
    }
}
